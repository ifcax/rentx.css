# Rentx Style


https://www.npmjs.com/package/@ifca/rentx.css
================================================================================================================================
v1.0.41 changes
1) npm -> pages.scss -> change .table-responsive from overflow: auto; to overflow-y: hidden;

================================================================================================================================
v1.0.40 changes
1) npm -> ifca-standard.css -> change .float-btn.secondary 
                                    from 
                                        bottom: 86px;
                                    to
                                        bottom: 95px;
2) npm -> ifca-standard.css -> change @media (min-width: 768px) .float-btn.secondary 
                                    from 
                                        bottom: 101px;
                                    to
                                        bottom: 110px;
3) npm -> styles.scss -> add .core-list .responsive-list 
4) npm -> styles.scss -> add .core-list .responsive-list.mat-list-base .mat-list-item                                      

================================================================================================================================
v1.0.39 changes
1) npm -> styles.scss -> add .font-red-color

================================================================================================================================
v1.0.38 changes
1) npm -> styles.scss -> add .date-col-f-12
2) npm -> styles.scss -> add .contact-list.responsive-list
3) npm -> styles.scss -> add .contact-list.responsive-list.mat-list-base .mat-list-item
4) npm -> styles.scss -> add .contact-list.responsive-list.mat-list-base .mat-list-item:not(:last-child)

================================================================================================================================
v1.0.37 changes
1) npm -> styles.scss -> add .desc-f-medium

================================================================================================================================
v1.0.36 changes
1) npm -> styles.scss -> add .card-container.mat-card .mat-card-header .mat-card-title.header-main-title
2) npm -> styles.scss -> add .card-container.mat-card .mat-card-header .mat-card-subtitle.header-main-infoline
3) npm -> styles.scss -> add .card-container-content-f
4) npm -> styles.scss -> add .status-booked-color

================================================================================================================================
v1.0.35 changes
1) npm -> styles.scss -> add .status-signed-color
2) npm -> styles.scss -> add .status-signed-color

================================================================================================================================
v1.0.34 changes
1) npm -> _variable.scss -> remove $status-inactive
2) npm -> _variable.scss -> remove $status-active
3) npm -> _variable.scss -> remove $status-approve
4) npm -> _variable.scss -> remove $status-reject
5) npm -> _variable.scss -> remove $status-draft
6) npm -> _variable.scss -> remove $status-pending
7) npm -> _variable.scss -> remove $status-darkRed
8) npm -> _variable.scss -> remove $status-lighBlue
9) npm -> _variable.scss -> add $status-blue
10) npm -> _variable.scss -> add $status-darkRed
11) npm -> _variable.scss -> add $status-red
12) npm -> _variable.scss -> add $status-green
13) npm -> _variable.scss -> add $status-lightGrey
14) npm -> _variable.scss -> add $status-lightOrange
15) npm -> _variable.scss -> add $status-black
16) npm -> _variable.scss -> add $status-darkBlue
17) npm -> _variable.scss -> add $status-lightBlue
18) npm -> styles.scss -> add .status-active-color
19) npm -> styles.scss -> add .status-in-progress-color
20) npm -> styles.scss -> add .status-terminated-color
21) npm -> styles.scss -> add .status-rejected-color
22) npm -> styles.scss -> add .status-drop-color
23) npm -> styles.scss -> add .status-verified-color
24) npm -> styles.scss -> add .status-approved-color
25) npm -> styles.scss -> add .status-resolved-color
26) npm -> styles.scss -> add .status-expired-color
27) npm -> styles.scss -> add .status-closed-color
28) npm -> styles.scss -> add .status-open-color
29) npm -> styles.scss -> add .status-submitted-color
30) npm -> styles.scss -> add .status-loo-color
31) npm -> styles.scss -> add .status-draft-color
32) npm -> styles.scss -> add .status-signed-color
33) npm -> styles.scss -> add .status-new-color
34) npm -> styles.scss -> add .status-changing-color

================================================================================================================================
v1.0.33 changes
1) npm -> styles.scss -> remove .subheader .mat-expansion-panel .header-value-blue
2) npm -> styles.scss -> remove .subheader .mat-expansion-panel .header-value
3) npm -> styles.scss -> remove .header-value(been overwrite)

================================================================================================================================
v1.0.32 changes
1) npm -> _variable.scss -> add $status-lighBlue: #0A00FF;
2) npm -> _variable.scss -> add $status-darkRed: #CC0021;
3) npm -> styles.scss -> change .mat-card .mat-card-header img
                                    from
                                        width: 20px;
                                        height: 20px;
                                    to
                                        width: 24px;
                                        height: 24px;
4) npm -> styles.scss -> change .mat-card .mat-card-header .mat-icon
                                    from
                                        font-size: 20px;
                                        width: 20px;
                                        height: 20px;
                                        color:$icon-blue;
                                    to
                                        font-size: 24px;s
                                        width: 25px;
                                        height: 26px;
                                        color:$icon-blue;

================================================================================================================================
v1.0.31 changes
1)recompile some page.scss and responsive.scss to page.css and responsive.css

================================================================================================================================
v1.0.30 changes
1) npm -> _variable.scss -> change $status-approve from #00E169; to #00B259;

================================================================================================================================
v1.0.29 changes
1) npm -> styles.scss -> remove .mat-expansion-panel-body .content-detail-container

================================================================================================================================
v1.0.28 changes
1) npm -> styles.scss -> add .card-section-container .card-section-container-header
2) npm -> styles.scss -> add .card-section-container .label-f
3) npm -> styles.scss -> add .card-section-container .value-f
4) npm -> styles.scss -> add .card-section-container .value-f-blue
5) npm -> styles.scss -> add .card-section-container:not(:last-child)
6) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-with-checkbox 
7) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-with-checkbox:last-child 
8) npm -> styles.scss -> add .date-col-f-10
9) npm -> styles.scss -> add .todo-info-line

================================================================================================================================
v1.0.27 changes
1) npm -> styles.scss -> add .dollar-icon-value

================================================================================================================================
v1.0.26 changes
1) npm -> styles.scss -> change .new 
                                    from 
                                        vertical-align: center;
                                    to 
                                        vertical-align: middle;
2) npm -> styles.scss -> change .approved 
                                    from 
                                        vertical-align: center;
                                    to 
                                        vertical-align: middle;
3) npm -> styles.scss -> change .pending-approval 
                                    from 
                                        vertical-align: center;
                                    to 
                                        vertical-align: middle;
4) npm -> styles.scss -> change .rejected 
                                    from 
                                        vertical-align: center;
                                    to 
                                        vertical-align: middle;

================================================================================================================================
v1.0.25 changes
1) npm -> styles.scss -> change .new 
                                    from 
                                        border: 1px solid #F47820;
                                        background-color: #F47820;
                                    to 
                                        border: 1px solid #A3437B;
                                        background-color: #A3437B;
2) npm -> styles.scss -> change .approved 
                                    from 
                                        border: 1px solid #FFC853;
                                        background-color: #FFC853;
                                    to 
                                        border: 1px solid #DA5A6A;
                                        background-color: #DA5A6A;
3) npm -> styles.scss -> change .pending-approval 
                                    from 
                                        border: 1px solid #F8F0D7;
                                        background-color: #F8F0D7;
                                    to 
                                        border: 1px solid #F5894D;
                                        background-color: #F5894D;
4) npm -> styles.scss -> change .rejected 
                                    from 
                                        border: 1px solid #34BCD2;
                                        background-color: #34BCD2;
                                    to 
                                        border: 1px solid #EDC436;
                                        background-color: #EDC436;

================================================================================================================================
v1.0.24 changes
1) npm -> styles.scss -> add .card-container .mat-card-header 
2) npm -> styles.scss -> add .card-container .mat-card-header.without-border
3) npm -> styles.scss -> add .card-container.mat-card .mat-card-header .mat-card-title
4) npm -> styles.scss -> add .card-container.mat-card .mat-card-header .mat-card-subtitle

================================================================================================================================
v1.0.23 changes
1) npm -> styles.scss -> add .desc-f-red
2) npm -> styles.scss -> change  .tbl-footer-row .mat-footer-cell
                                        from
                                            background-color: #f2f2f2 !important;
                                            text-align: right;
                                            font: 400 12px/18px Poppins !important;
                                        to
                                            background-color: #0019D8 !important;
                                            color: #FFFFFF;
                                            text-align: right;
                                            font: 400 12px/18px Poppins !important;

================================================================================================================================
v1.0.22 changes
1) npm -> styles.scss -> change .responsive-list.list-below-card from margin: 4px 6px 0px 6px; to margin: 6px 6px 0px 6px;

================================================================================================================================
v1.0.21 changes
1) npm -> styles.scss -> add .responsive-list.list-below-card 

================================================================================================================================
v1.0.20 changes
1) npm -> styles.scss -> remove .meter-detail-list .dim-bg
2) npm -> styles.scss -> add .tbl-footer-row .mat-footer-cell
3) npm -> styles.scss -> add .tbl-footer-row .tbl-total-label
4) npm -> styles.scss -> change .table-responsive .mat-header-cell.x-short,
.table-responsive .mat-cell.x-short
                                        from
                                            width: auto;
                                            min-width: 35px;
                                            max-width: 250px;
                                        to
                                            width: auto;
                                            min-width: 55px;
                                            max-width: 250px;
5) npm -> styles.scss -> add .table-responsive .mat-header-cell.xs-short,
.table-responsive .mat-cell.xs-short

================================================================================================================================
v1.0.19 changes
1) npm -> styles.scss -> add .table-responsive .mat-header-cell.text-center

================================================================================================================================
v1.0.18 changes
1) npm -> styles.scss -> change .scroll-container from height: 80vh to height: calc(100vh - 186px);
2) npm -> styles.scss -> change .icon-18
                                        from
                                            font-size: 18px !important;
                                            display: inline;
                                            vertical-align: middle;
                                        to
                                            font-size: 18px !important;
                                            width: 18px !important;
                                            height: 18px !important;
                                            vertical-align: middle;
3) npm -> styles.scss -> change from .input-detail >.left to .input-detail .left
4) npm -> styles.scss -> change from .input-detail >.right to .input-detail .right

================================================================================================================================
v1.0.17 changes
1) npm -> styles.scss -> change mat-raised-button.mat-button-disabled span
                                        from 
                                            color: rgba(0, 0, 0, 0.26) !important;
                                        to
                                            color:#FFFFFF
2) npm -> styles.scss -> add .mat-raised-button.mat-primary.mat-button-disabled,
.mat-raised-button.mat-accent.mat-button-disabled,
.mat-raised-button.mat-warn.mat-button-disabled,
.mat-raised-button.mat-button-disabled.mat-button-disabled
3) npm -> styles.scss -> change .table-responsive th
                                        from
                                            background-color: #6f7ffe;
                                            color: aliceblue;
                                        to
                                            background-color: #0019D8;
                                            color: #FFFFFF;
4) npm -> styles.scss -> change .table-responsive .mat-header-cell from text-align: center; to text-align: left;
5) npm -> styles.scss -> add .table-responsive .mat-header-cell.text-right 
6) npm -> styles.scss -> add radio-btn-input-container 

================================================================================================================================
v1.0.16 changes
1) npm -> styles.scss -> change from .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-container to .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list
2) npm -> styles.scss -> change from .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-container:last-child to .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list:last-child
3) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-expansion-panel-body .expanded-list-container 

================================================================================================================================
v1.0.15 changes
1) npm -> ifca-standard.css -> change .status-cookie from 2px 8px; to padding: 1px 12px;
2) npm -> styles.scss -> change .attachment-item-name
                                        from 
                                            font: 600 12px/18px Poppins;
                                            color: #212112;
                                        to
                                            font: 600 12px/18px Poppins;
                                            color: #212112;
                                            text-align: center;
3) npm -> styles.scss -> add .attachment-img-container .mat-mini-fab
                                        from 
                                            position: absolute;
                                            right: -19px;
                                            top: -19px;
                                        to
                                            position: absolute;
                                            right: -13px;
                                            top: -12px;
                                            width: 24px;
                                            height: 24px;
4) npm -> styles.scss -> add .attachment-display-error-container
5) npm -> styles.scss -> add .attachment-display-error
6) npm -> styles.scss -> add .attachment-display-error-f
7) npm -> styles.scss -> add .attachment-display-info-f
8) npm -> styles.scss -> add .m-r-8
9) npm -> styles.scss -> add .attachment-img-container .mat-mini-fab .mat-button-wrapper
10) npm -> styles.scss -> add .attachment-img-container .mat-mini-fab .mat-button-wrapper .mat-icon
11) npm -> styles.scss -> add .attachment-input form
12) npm -> ifca-standard.css -> add .status-cookie.blue
13) npm -> ifca-standard.css -> change .status-cookie.red
                                        from
                                            color: #ff1818;
                                            background: #ff181814;
                                        to
                                            color: #FF1B40;
                                            background: #FFE5E5;
14) npm -> _variable.scss -> add $font-red

================================================================================================================================
v1.0.14 changes
1) npm -> styles.scss -> remove .mat-form-field-disabled .mat-form-field-underline
2) npm -> ifca-standard.css -> change .status-cookie
                                        from
                                            border-radius: 100px;
                                            padding: 2px 10px;
                                            vertical-align: middle;
                                            font-size: 10px;
                                            font-weight: 500;
                                            background: #212121;
                                            color: #fff;
                                            width: fit-content;
                                            text-transform: uppercase;
                                        to
                                            border-radius: 4px;
                                            padding: 2px 8px;
                                            vertical-align: middle;
                                            font-size: 10px;
                                            font-weight: 500;
                                            background: #212121;
                                            color: #fff;
                                            width: fit-content;
3) npm -> styles.scss -> add .attachment-container
4) npm -> styles.scss -> add .attachment-container .attachment-item
5) npm -> styles.scss -> add .attachment-img-container
6) npm -> styles.scss -> add .attachment-img-container img
7) npm -> styles.scss -> add .attachment-img-container .mat-mini-fab
8) npm -> styles.scss -> add .attachment-item-name
9) npm -> styles.scss -> add .centre-profile-frame
10) npm -> styles.scss -> add .profile-upload-frame .profile-img
11) npm -> styles.scss -> add .profile-upload-frame .upload-icon
12) npm -> styles.scss -> add .mat-card .mat-card-header .mat-icon:not(:last-child)
13) npm -> styles.scss -> add .mat-card .mat-card-header img
14) npm -> styles.scss -> add .mat-card .mat-card-header img:not(:last-child)
15) npm -> styles.scss -> add .slide-toggle-input
16) npm -> styles.scss -> add .slide-toggle-input mat-label
17) npm -> styles.scss -> add .mat-card .mat-expansion-panel .mat-expansion-panel-header
18) npm -> styles.scss -> add .mat-card .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title
19) npm -> styles.scss -> add .mat-card .mat-expansion-panel .mat-expansion-panel-header:hover,
.mat-card .mat-expansion-panel .mat-expansion-panel-header:focus
20) npm -> styles.scss -> add .mat-card .mat-expansion-panel .mat-expansion-panel-body
21) npm -> styles.scss -> add .mat-card .mat-expansion-panel .mat-expansion-panel-body .mat-card
22) npm -> styles.scss -> add .responsive-list.mat-list-base .reminder-info-container.mat-list-item
23) npm -> styles.scss -> add .mat-list-base .reminder-info-container.mat-list-item .mat-list-icon
24) npm -> styles.scss -> add .mat-list-base .reminder-info-container.mat-list-item .mat-line
25) npm -> styles.scss -> add .reminder-info-f
26) npm -> styles.scss -> add .search-with-filter
27) npm -> styles.scss -> add .search-with-filter .mat-list:first-child
28) npm -> styles.scss -> add .search-with-filter .mat-list:not(:first-child)
29) npm -> styles.scss -> add .card-section-container .card-section-list
30) npm -> styles.scss -> add .card-section-container .card-section-list:not(:last-child)
31) npm -> styles.scss -> add .card-section-container .card-section-list:not(:first-child)
32) npm -> styles.scss -> add .card-section-container .card-section-list .card-section-list-item
33) npm -> styles.scss -> add ::ng-deep .mat-form-field-appearance-outline .mat-form-field-outline-end,
    ::ng-deep .mat-form-field-appearance-outline .mat-form-field-outline-start 
34) npm -> styles.scss -> add .check-box-container
35) npm -> styles.scss -> add .check-box-header
36) npm -> styles.scss -> add .desc-f span.mat-checkbox-label 
37) npm -> styles.scss -> add .check-box-content
38) npm -> styles.scss -> add .check-box-footer
39) npm -> styles.scss -> add .check-box-footer .mat-raised-button span
40) npm -> styles.scss -> add .check-box-footer button:not(:last-child)
41) npm -> styles.scss -> add .check-box-container .mat-checkbox-layout
42) npm -> styles.scss -> add .check-box-content-detail
43) npm -> styles.scss -> add .check-box-content .mat-checkbox
44) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-container
45) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-expansion-panel-body .mat-list-item.expanded-list-container:last-child


================================================================================================================================
v1.0.13 changes
1) npm -> styles.scss -> add .cancel-decoration

================================================================================================================================
v1.0.12 changes
1) npm -> styles.scss -> change .responsive-list.mat-list-base .mat-list-item from padding: 8px 12px !important; to padding: 10px 12px !important;
2) npm -> styles.scss -> change .responsive-list .with-expand-panel .mat-expansion-panel from padding: 8px 12px !important; to padding: 10px 12px !important;
3) npm -> styles.scss -> change .responsive-list.mat-list-base .mat-list-item.pos-list from   padding: 8px 0px 0px !important; to   padding: 10px 0px 0px !important;
4) npm -> styles.scss -> change .responsive-list .pos-list .mat-expansion-panel .mat-expansion-panel-header from    padding-bottom: 8px !important; to  padding-bottom: 10px !important;

================================================================================================================================
v1.0.11 changes
1) npm -> _variable.scss -> change $icon-blue: #5367FF; to $icon-blue: #0019D8;
2) npm -> ifca-standard.css -> change .search-bar-title from   font: 600 12px/14px Poppins !important; to  font: 600 12px/18px Poppins !important;

================================================================================================================================
v1.0.10 changes
1) npm -> styles.scss -> add .mat-accordion .mat-expansion-panel-spacing
2) npm -> styles.scss -> add .time-line-header-f
3) npm -> styles.scss -> remove .mat-tab-list 
4) npm -> styles.scss -> remove .responsive-list.mat-list-base .mat-list-item.dim-bg
5) npm -> styles.scss -> remove .dim-bg
6) npm -> styles.scss -> remove .dim-bg .mat-list-item-content
7) npm -> styles.scss -> change .dim-bg * from color: #8d6e6e !important; to  
8) npm -> styles.scss -> add .status-color-code-grey 
9)npm -> _variable.scss -> change $icon-blue: #0019D8; to $icon-blue: #5367FF;
10)npm -> _variable.scss -> change $icon-Red: #F72D2D; to $icon-Red: #FF1B40;
11)npm -> _variable.scss -> change $status-approve: #00BF0F; to $status-approve: #00E169;
12)npm -> _variable.scss -> $status-reject: #C91126; to $status-reject: #FF1B40;
13)npm -> _variable.scss -> add $expire-color: #9B9B9B;

================================================================================================================================
v1.0.9 changes
1) npm -> styles.scss -> add .responsive-list.mat-list-base .with-expand-panel.mat-list-item
2) npm -> styles.scss -> add .responsive-list .with-expand-panel .mat-expansion-panel
3) npm -> styles.scss -> change .mat-list-base .mat-list-item .mat-list-avatar.list-thumbnail > img added object-fit: cover;
4) npm -> styles.scss -> add .mat-expansion-panel .mat-expansion-panel-header:hover

================================================================================================================================
v1.0.8 changes
1) npm -> styles.scss -> remove .responsive-list .mat-expansion-panel.with-badge .mat-content

================================================================================================================================
v1.0.7 changes
1) npm -> styles.scss -> add .profile-frame
2) npm -> styles.scss -> add .profile-frame > img
3) npm -> styles.scss -> add .mat-card .mat-card-header .mat-icon
4) npm -> _variable.scss -> change from $icon-Blue: #5367FF; to $icon-blue: #0019D8;
5) npm -> _variable.scss -> change from $icon-DarK-Blue: to $icon-darkBlue: 

================================================================================================================================
v1.0.6 changes
1) npm -> styles.scss -> add .sidebar-popup-menu-font
2) npm -> styles.scss -> add .sidebar-popup-menu-icon
3) npm -> styles.scss -> add .font-desc-orange
4) npm -> styles.scss -> change .footer-menu button
                                    from 
                                        width: 100%;
                                        margin: 0 16px;
                                        max-width: 200px;
                                        flex-grow: 1;
                                    to
                                        width: 100%;
                                        margin: 0 15px;
                                        max-width: 175px;
                                        flex-grow: 1;
5) npm -> styles.scss -> add .copyright-footer
6) npm -> styles.scss -> add .copyright-footer .copyright-app-font
7) npm -> styles.scss -> add @media (min-width: 768px) {
    .copyright-footer{
        padding: 8px 12px;
        background: #E5E5E5;
        font: 500 12px/18px Poppins;
        color: #000000
    }

    .footer-menu{
        justify-content: flex-end;
    }
}
8) npm -> styles.scss -> change .responsive-content remove height: 100%;
9) npm -> ifca-standard.css -> change .footer-menu
                                    from
                                        padding: 10px 0px;
                                        width: 100%;
                                        text-align: center;
                                        position: fixed;
                                        bottom: 0;
                                        left: 0;
                                        background: #fff;
                                        height: 56px;
                                        z-index: 10;
                                        cursor: pointer;
                                        box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0), 0 1px 10px 2px rgba(0, 0, 0, 0.05), 0 0px 5px 0 rgba(0, 0, 0, 0.1);
                                        /* remove blue highlight */
                                        -webkit-tap-highlight-color: transparent !important;
                                        -moz-tap-highlight-color: transparent !important;
                                        -o-tap-highlight-color: transparent !important;
                                        tap-highlight-color: transparent !important;
                                        justify-content: space-evenly;
                                        display:flex;
                                        flex-flow: row nowrap;
                                    to
                                    padding: 10px 0px;
                                        width: 100%;
                                        text-align: center;
                                        position: relative;
                                        background: #fff;
                                        z-index: 10;
                                        cursor: pointer;
                                        box-shadow: 0 3px 1px -2px rgba(0, 0, 0, 0), 0 1px 10px 2px rgba(0, 0, 0, 0.05), 0 0px 5px 0 rgba(0, 0, 0, 0.1);
                                        /* remove blue highlight */
                                        -webkit-tap-highlight-color: transparent !important;
                                        -moz-tap-highlight-color: transparent !important;
                                        -o-tap-highlight-color: transparent !important;
                                        tap-highlight-color: transparent !important;
                                        justify-content: space-evenly;
                                        display:flex;
                                        flex-flow: row nowrap;
10) npm -> ifca-standard.css -> add .footer.sticky
11) npm -> ifca-standard.css -> remove @media (min-width: 768px) {
    .footer-menu {
        width: calc(100% - 240px - 30px);
        left: unset;
        right: 15px;
        bottom: 0px;
        transition: 0.3s;
    }
} 
12) npm -> ifca-standard.css -> change .responsive-content add height: calc(100% - 24px);
13) npm -> ifca-standard.css -> change @media (min-width: 768px) { .responsive-content} add height: calc(100% - 120px);
14) npm -> ifca-standard.css -> change responsive-list.mat-list-base .active-page.mat-list-item from background: #0019D8; to background: #0019D8 !important;
15) npm -> styles.scss -> change .subheader .mat-expansion-panel .header-label remove font:600 10px/16px Poppins !important;
16) npm -> ifca-standard.css -> change .float-btn,
.floating-btn bottom:15px to bottom:40px
17) npm -> styles.scss -> add .cdk-global-scrollblock
18) npm -> styles.scss -> add 
19) npm -> styles.scss -> add 
20) npm -> styles.scss -> add 

================================================================================================================================
v1.0.5 changes
1) npm -> styles.scss -> change red-badge from 
                                                position: absolute;
                                                top: 7px;
                                                right: 7px;
                                            to
                                                position: absolute;
                                                top: 7px;
                                                right: unset;
                                                margin-left: -7px;
2) npm -> styles.scss -> change mat-list-base .mat-list-item .mat-list-avatar.list-thumbnail from position: relative; to position: unset;
3) npm -> sidebar.scss -> change mat-sidenav from 350px to 240px 
4) npm -> ifca-standard.css -> change 
@media (min-width: 768px) {
    .responsive-content {
        width: calc(100% - 350px);  to width: calc(100% - 240px); 
    }
}

5) npm -> ifca-standard.css -> change  
@media (min-width: 768px) {
    .footer-menu {
        width: calc(100% - 350px - 30px); to width: calc(100% - 240px - 30px);
        left: unset;
        right: 15px;
        bottom: 0px;
        transition: 0.3s;
    }
}
6) npm -> styles.scss -> change .page-content from margin-top: 64px; to padding-top: 64px;
7) npm -> styles.scss -> add mat-form-label-font
8) npm -> styles.scss -> add mat-form-value-font
9) npm -> styles.scss -> remove .ifca-general-header .bread-crumb ,.ifca-general-header .bread-crumb span
10) npm -> styles.scss -> add .ifca-general-header .bread-crumb
11) npm -> styles.scss -> change .ifca-general-header .bread-crumb .active
                                from 
                                    font: 600 9px/12px Poppins;
                                    color: #ffffff;
                                    padding-top: 2px;
                                to
                                    font: 600 10px/14px Poppins;
                                    color: #ffffff;
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis; 
12) npm -> styles.scss -> add .ifca-general-header .bread-crumb .left-bread-crumb
13) npm -> styles.scss -> add .ifca-general-header .bread-crumb .right-bread-crumb
14) npm -> styles.scss -> change .ifca-general-header .selected-obj 
                                from
                                    font: 600 15px/18px Poppins;
                                    color: #ffb11f;
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis;
                                    margin-top: -2px;
                                to
                                    font: 600 12px/16px Poppins;
                                    color: #ffb11f;
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis; 

15) npm -> styles.scss -> change .ifca-general-header .domain
                                from
                                    font: 400 10px/16px Poppins;
                                    color: #ffffff;
                                    margin-top: -1px;
                                to
                                    font: 400 10px/14px Poppins;
                                    color: #ffffff;
16) npm -> styles.scss -> add .switch-icon
17) npm -> styles.scss -> change button.menu-burger.mat-icon-button 
                                from
                                    color: #fff;
                                    background-color: #7e8dff !important;
                                    border-radius: 0% !important;
                                    width: 30px;
                                    height: 30px;
                                    line-height: 30px;
                                to
                                    color: #fff;
                                    background-color: #7e8dff !important;
                                    border-radius: 2px !important;
                                    width: 22px;
                                    height: 22px;
                                    line-height: 22px;
                                    margin-right: 6px;
18) npm -> styles.scss -> add button.menu-burger.mat-icon-button  .mat-icon
19) npm -> styles.scss -> add .ifca-general-header.sub-header
20) npm -> styles.scss -> add @media (max-width: 767px) {.top-header }
21) npm -> styles.scss -> add @media (min-width: 768px) {.ifca-general-header.sub-header .menu-burger.menu-logo}
22) npm -> styles.scss -> remove button.menu-logo.mat-icon-button 
23) npm -> sidebar.scss -> remove  #snav .mat-drawer-inner-container .mat-nav-list .mat-list-item  &:hover 
24) npm -> styles.scss -> change .responsive-list.mat-list-base .active-page.mat-list-item from background: #bfd3ff; to background: #0019D8 ;
25) npm -> styles.scss -> change .selected-container remove padding: 0 6px;
26) npm -> styles.scss -> add .side-menu-font
27) npm -> styles.scss -> add .active-page .side-menu-font
28) npm -> styles.scss -> add .top-header .title
29) npm -> styles.scss -> add .top-header .mat-icon
30) npm -> styles.scss -> change .ifca-general-header from padding: 8px 10px !important; to padding: 4px 12px !important;

================================================================================================================================
v1.0.4 changes
1) npm -> styles.scss -> change pos-detail-attachment-filename-f from font: 600 12px/18px Poppins; to font: 500 12px/18px Poppins;
2) npm -> styles.scss -> add .font-lightBlue-color
3) npm -> styles.scss -> add .font-darkBlue-color
4) npm -> styles.scss -> add .font-black-color
5) npm -> styles.scss -> add .font-label-color
6) npm -> styles.scss -> add .font-desc-color
7) npm -> styles.scss -> add .attachment-list-container .attachment-list
8) npm -> styles.scss -> add .attachment-filename-container
9) npm -> styles.scss -> add .attachment-list-container .attachment-list .attachment-detail
10) npm -> styles.scss -> add .attachment-list-container .attachment-list:first-child .attachment-detail
11) npm -> styles.scss -> add .attachment-list-container .attachment-list:last-child .attachment-detail
12) npm -> styles.scss -> add .dialog-content .attachment-list-container .attachment-list:first-child .attachment-detail
13) npm -> styles.scss -> add .dialog-content .attachment-list-container .attachment-list:last-child .attachment-detail
14) npm -> styles.scss -> add .dialog-content .attachment-list-container .attachment-list
15) npm -> styles.scss -> add .dialog-content .attachment-list-container.quotation-list .attachment-list:last-child .attachment-detail
16) npm -> styles.scss -> add .dialog-content .responsive-list.mat-list-base .mat-list-item
17) npm -> styles.scss -> add .dialog-content .responsive-list
18) npm -> styles.scss -> add .dialog-content .mat-card .mat-card-content
19) npm -> styles.scss -> add .dialog-content .section-label
20) npm -> styles.scss -> add .dialog-content.list-container
21) npm -> styles.scss -> add .list-container .responsive-list.mat-list-base .mat-list-item
22) npm -> styles.scss -> add .list-container .responsive-list.mat-list-base .mat-list-item.selected-item
23) npm -> styles.scss -> add .list-container .responsive-list.mat-list-base .mat-list-item .mat-list-item-content
24) npm -> styles.scss -> add .list-container .responsive-list.mat-list-base .mat-list-item:first-child .mat-list-item-content
25) npm -> styles.scss -> add .list-container .responsive-list.mat-list-base .mat-list-item:last-child .mat-list-item-content
26) npm -> styles.scss -> add .filter-option .mat-list-base
27) npm -> styles.scss -> add .filter-option .radio-btn-list-container > .mat-radio-button
28) npm -> styles.scss -> add .filter-option .radio-btn-list-container > .mat-radio-button:first-child
29) npm -> styles.scss -> add .filter-option .radio-btn-list-container > .mat-radio-button:last-child
30) npm -> styles.scss -> add .filter-option .mat-list-base .mat-list-item
31) npm -> styles.scss -> add .filter-option .mat-list-item-content
32) npm -> styles.scss -> add .filter-option .mat-list-option
33) npm -> styles.scss -> add .dialog-content .section-label.first-section-label
34) npm -> styles.scss -> add .filter-option .mat-radio-label-content
35) npm -> styles.scss -> add .filter-option .mat-list-option:first-child .mat-list-item-content
36) npm -> styles.scss -> add .filter-option .mat-list-option:last-child .mat-list-item-content
37) npm -> styles.scss -> add .dialog-header-button.mat-raised-button
38) npm -> styles.scss -> add .dialog-header-button.mat-raised-button span
39) npm -> styles.scss -> add .icon-16
40) npm -> styles.scss -> add .dialog-header.responsive-list.mat-list-base .mat-list-item
41) npm -> styles.scss -> add .side-title
42) npm -> styles.scss -> change header-top-right-font from color: #0019D8; to color: #001088; 
43) npm -> styles.scss -> add filter-option .mat-list-base .mat-list-option .mat-list-text

================================================================================================================================
v1.0.3 changes
1) npm -> styles.scss -> change to user variable
2) npm -> styles.scss -> remove .responsive-list .pos-list .list-meter.dim-bg * 

================================================================================================================================
v1.0.2 changes
1) npm -> styles.scss -> remove .responsive-list .pos-list .list-meter.dim-bg .list-meter-detail
2) npm -> styles.scss -> add .responsive-list .pos-list .list-meter.dim-bg:first-child .list-meter-detail
3) npm -> styles.scss -> remove   .responsive-list .pos-list .list-meter.dim-bg .list-meter-detail .desc-f-black-medium,
.responsive-list .pos-list .list-meter.dim-bg .list-meter-detail .desc-f-blue-medium
4) npm -> styles.scss -> add .dim-bg *

================================================================================================================================
v1.0.1 changes
1) npm -> styles.scss -> change @media (min-width: 769px) {
  /* new Standard */
  .with-fixed-subheader-with-search,
  .with-fixed-subheader-1-line,
  .with-fixed-subheader-1-line-with-search,
  .with-fixed-subheader-mat-icon-1-line,
  .with-fixed-subheader-mat-icon-2-line,
  .with-fixed-subheader-mat-icon-1-line-with-search,
  .with-fixed-subheader-mat-icon-2-line-with-search,
  .with-fixed-subheader-2-line-with-search,
  .with-fixed-subheader-2-line,
  .with-fixed-subheader-mat-icon-2-line-with-search-margin,
  .with-fixed-subheader-mat-icon-1-line-with-opp-subheader,
  .with-fixed-subheader-with-opp-subheader,
  .with-fixed-subheader-dynamic {
    margin-top: 6px !important; from 7 change to 6
  }
}
2) npm -> styles.scss -> change .detail-card .info-container from background-color: #e8e8e8; padding: 0px 12px; to background-color: #ffffff; padding: 8px 12px;
3) npm -> add ifca-scss/_variable.scss 
4) npm -> styles.scss -> add @import "_variable.scss";
5) npm -> styles.scss -> change responsive-list.subheader.mat-list-base .mat-list-item from background: #bfd3ff; to background: $subheader-Blue;
6) npm -> styles.scss -> add .header-top-section 
7) npm -> styles.scss -> add .header-top-right-font
8) npm -> styles.scss -> add .header-top-left-font
9) npm -> styles.scss -> add .header-btm-font
10) npm -> styles.scss -> add .header-expand-icon 
11) npm -> styles.scss -> add .header-expand-icon.mat-icon
12) npm -> styles.scss -> add .mat-expansion-panel .mat-expansion-panel-header.mat-expanded .header-expand-icon
13) npm -> styles.scss -> add .subheader .mat-expansion-panel .mat-expansion-panel-header
14) npm -> styles.scss -> add .subheader .mat-expansion-panel
15) npm -> styles.scss -> add .subheader .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title
16) npm -> styles.scss -> add .subheader .mat-expansion-panel .mat-expansion-panel-header span
17) npm -> styles.scss -> add .subheader .mat-expansion-panel .mat-expansion-panel-body
18) npm -> styles.scss -> add .subheader .mat-expansion-panel .mat-expansion-panel-header:hover
19) npm -> styles.scss -> add .subheader .mat-expansion-panel .header-label
20) npm -> styles.scss -> add .subheader .mat-expansion-panel .header-value
21) npm -> styles.scss -> add .subheader .mat-expansion-panel .header-value-blue
22) npm -> styles.scss -> add p-t-8
23) npm -> styles.scss -> add p-t-6
24) npm -> styles.scss -> change .responsive-list .title-f from 
                            font: 600 12px/14px Poppins !important;
                            line-height: 1rem !important;
                            display: inline-block !important;
                            color: #0019d8;
                        to
                            font: 600 12px/18px Poppins !important;
                            display: inline-block !important;
                            color: #212121;
25) npm -> styles.scss -> remove .mat-list-base .mat-list-item .mat-list-avatar from
                            .responsive-list.subheader .mat-list-icon,
                            .mat-list-base .mat-list-item .mat-list-icon,
                            .mat-list-base .mat-list-option .mat-list-icon,
                            .mat-list-base .mat-list-item .mat-list-avatar,
                            .mat-list-base .mat-list-option .mat-list-avatar 
26) npm -> styles.scss -> add .mat-list-base .mat-list-item .mat-list-avatar
27) npm -> styles.scss -> change title-f from font: 600 12px/14px Poppins !important;    line-height: 1rem !important; to font: 600 12px/18px Poppins !important;
28) npm -> styles.scss -> change title-f-black from  font: 600 12px/16px Poppins !important; to font: 600 12px/18px Poppins !important;
29) npm -> styles.scss -> change title-f-blue from  font: 600 12px/14px Poppins !important; line-height: 1rem !important; color: #0019d8; to font: 600 12px/18px Poppins !important; color: #0019d8;
30) npm -> styles.scss -> change .detail-card.expansion .responsive-list .title-f from  
                            font: 600 12px/14px Poppins !important;
                            line-height: 1rem !important;
                            color: #212121 !important;
                        to 
                            font: 600 12px/18px Poppins !important;
                            color: #212121 !important;
31) npm -> styles.scss -> change responsive-list .title-f.meter-attachment-info-f-fw600 from  font: 600 14px/21px Poppins !important; to font: 600 14px/20px Poppins !important;
32) npm -> styles.scss -> change  responsive-list .title-f.meter-attachment-info-f-fw400 from font: 400 14px/21px Poppins !important; to font: 400 14px/20px Poppins !important;
33) npm -> styles.scss -> change .detail-card.expansion .responsive-list.meter-utility .title-f from 
            font: 600 12px/14px Poppins !important;
            line-height: 1rem !important;
            color: #0019d8 !important;
        to
            font: 600 12px/18px Poppins !important;
            color: #0019D8 !important;
34) npm -> styles.scss -> change mat-list-base .mat-list-item .mat-list-icon
from  padding: 5px 2px !important; to padding: 0px !important;
35) npm -> styles.scss -> change two-n-three-row-list .desc-f from
 font: 400 10px/11px Poppins !important; to font: 400 10px/16px Poppins !important;
36) npm -> styles.scss -> change two-n-three-row-list .title-account from 
                        font: 600 12px/14px Poppins !important;
                        line-height: 1.6rem !important;
                        display: inline-block;
                    to
                        font: 600 12px/18px Poppins !important;
                        display: inline-block;
37) npm -> styles.scss -> change title-account-top from 
                            font: 600 12px/14px Poppins !important;
                            line-height: 1rem !important;
                            /*equal to 16px*/
                            display: inline-block;
                            padding-right: 10px;
                            color: #001088 !important;
                        to
                            font: 600 12px/18px Poppins !important;
                            display: inline-block;
                            padding-right: 10px;
                            color: #001088 !important;
38) npm -> styles.scss -> change title-account-bottom from  
                            font: 300 10px/11px Poppins !important;
                            line-height: 1rem !important;
                            display: inline-block;
                            padding-right: 10px;
                            color: #212121 !important;
                        to
                            font: 300 10px/16px Poppins !important;
                            display: inline-block;
                            padding-right: 10px;
                            color: #212121 !important;
39) npm -> styles.scss -> change title-account-top-right from
                            font: 600 12px/14px Poppins !important;
                            line-height: 1.6rem !important;
                            display: inline-block;
                            color: #0019d8 !important;
                        to
                            font: 600 12px/18px Poppins !important;
                            display: inline-block;
                            color: #0019D8 !important; 
40) npm -> styles.scss -> change title-account-code from
                            font: 300 10px/11px Poppins !important;
                            line-height: 1.6rem !important;
                            display: inline-block;
                            padding-left: 10px;
                        to
                            font: 300 10px/16px Poppins !important;
                            display: inline-block;
                            padding-left: 10px;
41) npm -> styles.scss -> change .desc-f.desc-f-blue-right from font: 600 10px/15px Poppins !important; to font: 600 10px/16px Poppins !important;
42) npm -> styles.scss -> change desc-f-blue-bold from 
                            display: block !important;
                            color: #0019d8;
                            font: 600 10px/16px Poppins !important;
                        to 
                            color: #0019D8;
                            font: 600 10px/16px Poppins !important;
43) npm -> styles.scss -> add .mat-list-base .mat-list-item .mat-list-avatar.list-thumbnail
44) npm -> styles.scss -> add .mat-list-base .mat-list-item .mat-list-avatar.list-thumbnail > img
45) npm -> styles.scss -> add .dollar-gap
46) npm -> styles.scss -> change .status-color-code-yellow from color: #f16400 !important; to color: #FF9800 !important;
47) npm -> styles.scss -> change .status-color-code-red from color: #ff365d !important; to color: #C91126 !important;
48) npm -> styles.scss -> add .status-color-code-orange 
49) npm -> styles.scss -> add .second-col-info
50) npm -> styles.scss -> add .contact-f 
51) npm -> styles.scss -> add .responsive-list .mat-expansion-panel
52) npm -> styles.scss -> add .responsive-list .mat-expansion-panel .mat-expansion-panel-header
53) npm -> styles.scss -> add .responsive-list .mat-expansion-panel .mat-expansion-panel-body
54) npm -> styles.scss -> add .responsive-list .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-panel-header-title
55) npm -> styles.scss -> add .contact-icon.contact-icon-gap
56) npm -> styles.scss -> add .responsive-list .mat-expansion-panel .mat-expansion-panel-header .mat-expansion-indicator
57) npm -> styles.scss -> add .responsive-list .pos-list .list-meter
58) npm -> styles.scss -> add .responsive-list .pos-list .list-meter .list-meter-detail
59) npm -> styles.scss -> add .responsive-list .list-meter:last-child
60) npm -> styles.scss -> add .responsive-list .pos-list .list-meter .list-meter-detail > div
61) npm -> styles.scss -> add .responsive-list .pos-list .mat-expansion-panel .mat-expansion-panel-header
62) npm -> styles.scss -> add .responsive-list .pos-list .mat-expansion-panel .mat-expansion-panel-body
63) npm -> styles.scss -> add .pos-detail-title-f
64) npm -> styles.scss -> add .pos-detail-value-f
65) npm -> styles.scss -> add .pos-detail-summary-title-f
66) npm -> styles.scss -> add .pos-detail-summary-total-sales-title-f
67) npm -> styles.scss -> add .pos-detail-summary-total-sales-value-f 
68) npm -> styles.scss -> add .pos-detail-attachment-filename-f
69) npm -> styles.scss -> add .pos-detail-attachment-title-f 
70) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-list
71) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail
72) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-attachment
73) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-attachment-container .pos-detail-list:last-child .pos-detail-attachment
74) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-attachment-container
75) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-summary .pos-detail
76) npm -> styles.scss -> add .responsive-list .pos-list .pos-detail-summary
77) npm -> styles.scss -> add .news-container
78) npm -> styles.scss -> add .responsive-list .mat-expansion-panel.with-badge
79) npm -> styles.scss -> add .responsive-list .mat-expansion-panel.with-badge .mat-content
80) npm -> styles.scss -> add .news-img-container
81) npm -> styles.scss -> add .news-img-responsive
82) npm -> ifca-standard.css -> change .footer-menu add display:flex; flex-flow: row nowrap;
83) npm -> styles.scss -> add .mat-card-header
84) npm -> styles.scss -> add .mat-card .mat-card-header .mat-card-title
85) npm -> styles.scss -> add .card-content-list > div
86) npm -> styles.scss -> add .card-content-list
87) npm -> styles.scss -> add .card-content-list:first-child 
88) npm -> styles.scss -> add .card-content-list:last-child 
89) npm -> styles.scss -> add .content-detail-container > div
90) npm -> styles.scss -> add .content-detail-container > div:last-child
91) npm -> styles.scss -> add .content-detail-container > div:nth-last-child(2).left
92) npm -> styles.scss -> add .content-detail-container > div
93) npm -> styles.scss -> add .content-detail-container > div.left 
94) npm -> styles.scss -> add .content-detail-container > div.right
95) npm -> styles.scss -> add .list-thumbnail
96) npm -> styles.scss -> add .list-info-container
97) npm -> styles.scss -> add .flex-container-wrap
98) npm -> styles.scss -> add .flex-container-nowrap
99) npm -> styles.scss -> add .radio-btn-list-container > .mat-radio-button
100) npm -> styles.scss -> add .radio-btn-list-container > .mat-radio-button:first-child
101) npm -> styles.scss -> add .radio-btn-list-container > .mat-radio-button:last-child
102) npm -> styles.scss -> add .mat-radio-group .mat-radio-label-content
103) npm -> styles.scss -> add .mat-radio-label-content.mat-radio-label-before
104) npm -> styles.scss -> add .input-detail
105) npm -> styles.scss -> add .input-detail > .mat-form-field
106) npm -> styles.scss -> add .input-detail > .left
107) npm -> styles.scss -> add .input-detail > .right
108) npm -> styles.scss -> add .mat-card .mat-card-content 
109) npm -> styles.scss -> add .header-label
110) npm -> styles.scss -> add .header-value
111) npm -> styles.scss -> add .header-value-bold 
112) npm -> styles.scss -> add .dialog-header .infoline-list:last-child .infoline-seperator
113) npm -> styles.scss -> add .dialog-header .infoline-list:last-child 
114) npm -> styles.scss -> add .icon-12
115) npm -> styles.scss -> add .red-badge
116) npm -> styles.scss -> add .mat-badge-content
117) npm -> styles.scss -> add .mat-expansion-panel-body .content-detail-container
118) npm -> styles.scss -> change .desc-f-blue-medium
    display: block !important;
    color: $font-lightBlue;
    font: 500 10px/16px Poppins !important;
    to
    color: $font-lightBlue;
    font: 500 10px/16px Poppins !important;
119) npm -> styles.scss -> add button.mat-menu-item
120) npm -> styles.scss -> add .icon-14
121) npm -> styles.scss -> add .icon-blue
122) npm -> styles.scss -> add .responsive-list .mat-expansion-panel .mat-expansion-panel-body .content-detail-container
123) npm -> styles.scss -> add .responsive-list.mat-list-base .mat-list-item.pos-list
124) npm -> styles.scss -> change desc-f from display: block !important; to display: inline-block !important;

================================================================================================================================
v0.0.42 changes
1) npm -> styles.scss -> change .new from #250ec1 to #F47820
2) npm -> styles.scss -> change .approved from #2e46fe to #FFC853
3) npm -> styles.scss -> change .pending-approval from #4b99b6 to #F8F0D7
4) npm -> styles.scss -> change .rejected from #78a9ff to #34BCD2

================================================================================================================================
v0.0.39 changes
1) npm -> styles.scss -> remove .mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:hover,
.mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:not(hover)
2) npm -> styles.scss -> remove mat-expansion-panel .mat-expansion-panel-header:hover
3) npm -> styles.scss -> add expansion-frame .mat-expansion-panel
4) npm -> styles.scss -> add expansion-frame .mat-expansion-panel .mat-expansion-panel-header
5) npm -> styles.scss -> add expansion-frame .mat-expansion-panel .mat-expansion-panel-body
6) npm -> styles.scss -> add expansion-frame .mat-expansion-panel .mat-expansion-panel-body .mat-card.mat-card.mat-focus-indicator
7) npm -> styles.scss -> add detail-card.expansion .mat-expansion-panel-header

================================================================================================================================
v0.0.38 changes
1) npm -> styles.scss -> change .header-value from font: 500 12px/12px Poppins !important; to  font: 500 12px/16px Poppins !important; 
2) npm -> styles.scss -> change .header-value-blue from font: 500 10px/11px Poppins !important; to font: 500 10px/14px Poppins !important;
3) npm -> styles.scss -> change .meter-attachment .header-value, .meter-attachment .header-value-blue from font: 600 12px/12px Poppins !important; to font: 600 12px/16px Poppins !important;


================================================================================================================================
v0.0.37 changes
1)compile changed scss to css
2) npm -> app.scss -> change .m-t-0 from margin-top: 0px; to margin-top: 0px; !important;
3) npm -> styles.scss -> add .contact-icon
4) npm -> styles.scss -> change span.mat-checkbox-label remove color


================================================================================================================================
v0.0.36 changes
1) npm -> styles.scss -> change dialog-content.scrollable from overflow-y: scroll; to overflow-y: auto;
2) npm -> styles.scss -> change mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:hover,
                            .mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:not(hover) added !important for the background
3) npm -> styles.scss -> add .mat-list-base .mat-list-item .mat-list-icon.profile-img-icon
4) npm -> styles.scss -> add .responsive-list.single .mat-list-icon -> font-size: 22px !important; move to after mat-list-base .mat-list-item .mat-list-icon
5) npm -> styles.scss -> add with-fixed-subheader-mat-icon-2-line-with-search-margin
6) npm -> styles.scss -> add with-fixed-subheader-mat-icon-1-line-with-opp-subheader
6) npm -> styles.scss -> add desc-f-blue-medium
7) npm -> styles.scss -> change desc-f-blue-requestType from font: 600 10px/11px Poppins !important; to font: 600 10px/16px Poppins !important;
8) npm -> styles.scss -> add .with-fixed-subheader-with-opp-subheader
9) npm -> styles.scss -> add .title-f-black
10) npm -> styles.scss -> add clear-box-shadow.responsive-list.mat-list-base .mat-list-item
11) npm -> styles.scss -> add mat-list-base .mat-list-item .mat-list-icon.profile-detail-img-icon
12) npm -> styles.scss -> add with-fixed-subheader-dynamic
13) npm -> styles.scss -> add .mat-list-base .mat-list-item .mat-list-icon.profile-detail-img-icon

================================================================================================================================
v0.0.35 changes
1) npm -> styles.scss -> add .mat-datepicker-content-touch
2) npm -> styles.scss -> add .mat-expansion-panel .mat-expansion-panel-header:hover
3) npm -> styles.scss -> add .mat-calendar-body-selected
4) npm -> styles.scss -> modify .ifca-general-header .bread-crumb,
                                .ifca-general-header .bread-crumb span {
                                    font: 400 9px/12px Poppins;
                                    color: #ffffff;
                                    padding-top: 2px;
                                    white-space: nowrap;
                                    overflow: hidden;
                                    text-overflow: ellipsis;
                                    width: 95vw;
                                }
5) npm -> styles.scss -> modify .white-input * from color: white !important; to color: white;

================================================================================================================================
v0.0.34 changes
1) npm -> styles.scss -> add .mat-raised-button.mat-button-disabled span
2) npm -> styles.scss -> change button.mat-raised-button.btn-default from padding: 0px 15px !important; to padding: 0px 25px !important;
3) npm -> styles.scss -> change button.mat-raised-button.btn-blue from padding: 0px 30px !important; to padding: 0px 25px !important;
4) npm -> styles.scss -> change button.mat-raised-button.btn-red from padding: 0px 30px !important; to padding: 0px 25px !important;
5) npm -> styles.scss -> rename .mat-list-base .mat-list-item.meter-reading to .meter-detail-list .mat-list-base .mat-list-item.meter-reading
6) npm -> styles.scss -> change desc-f-blue from font: 400 10px/11px Poppins !important; to  font: 400 10px/16px Poppins !important;
7) npm -> styles.scss -> change desc-f-blue-bold from font: 600 10px/11px Poppins !important; to  font: 600 10px/16px Poppins !important;
8) npm -> styles.scss -> change desc-f-blue-bold-underline from font: 600 10px/11px Poppins !important; to font: 600 10px/16px Poppins !important;
9) npm -> styles.scss -> .table-responsive .mat-header-cell.x-short, .table-responsive .mat-cell.x-short change from min-width: 60px; to min-width: 35px;

================================================================================================================================
v0.0.33 changes
1) npm -> styles.scss -> add .with-fixed-subheader-1-line-with-search
2) npm -> styles.scss -> .scrollable-> remove margin top 16px and added padding 8px 0px
3) npm -> styles.scss -> .responsive-list .meter-detail-list-last change from  padding: 12px 8px 4px 8px !important; to padding: 12px 12px 0px 12px !important;
4) npm -> styles.scss -> .responsive-list.meter-detail-list .mat-card-header change from  padding: 8px 8px 0px 8px !important; to padding: 8px 12px 0px 12px !important;
5) npm -> styles.scss -> add .meter-detail-list .mat-list-base .mat-list-item
6) npm -> styles.scss ->  change detail-card .info-container ->  from padding: 0px 16px; to padding: 0px 12px;
7) npm -> styles.scss -> change .service-request-desc from padding: 14px 12px; to padding: 14px 0px;
8) npm -> styles.scss -> add .service-request-desc:first-child 
9) npm -> styles.scss -> remove .service-request-desc .m-b-14:last-child and add .service-request-desc:last-child
10) npm -> styles.scss -> add .service-request-list-item
11) npm -> styles.scss -> add .service-request-desc:last-child:first-child 
12) npm -> styles.scss -> change check-date from margin-left: 30px; to padding-left: 30px;
13) npm -> styles.scss -> change button.mat-raised-button.btn-default from padding: 0px 30px !important; to padding: 0px 15px !important;
14) npm -> styles.scss -> remove mat-dialog-container -> padding: 16px 16px 0px !important; 
15) npm -> styles.scss -> remove scrollable -> margin: -16px -16px 0px; overflow-y: scroll; max-height: 320px; 
                        rename to dialog-content
                        add padding: 10px 12px !important;
16) npm -> styles.scss -> rename dialog-buttons to dialog-actions and dialog-buttons button to dialog-footer button
17) npm -> styles.scss -> add .font-16-weight-400
18) npm -> styles.scss -> change doc-error-style font to font: 400 11px/14px Poppins !important;
19) npm -> ifca-standard.css -> change @media (min-width: 768px) .footer-menu from width: calc(100% - 349px); to width: calc(100% - 349px - 30px);
20) npm -> ifca-standard.css -> add  .footer-menu  -> justify-content: space-evenly;
21) npm -> styles.scss -> add  .footer-menu button ->  width: 100%;  margin: 0 8px;  max-width: 200px;  flex-grow: 1;
}
22) npm -> styles.scss -> remove mat-raised-button.mat-primary -> background-color: #0019d8 !important;
23) npm -> styles.scss -> add mat-raised-button span  and remove mat-raised-button.mat-primary span


================================================================================================================================
v0.0.32 changes
1) npm -> styles.scss -> add .with-fixed-subheader-2-line-with-search,
                             .with-fixed-subheader-2-line
2) npm -> styles.scss -> add #DailySalesUse
3) npm -> styles.scss -> add .m-t-6
4) npm -> styles.scss -> .font-14-weight-500
5) npm -> styles.scss -> .mat-form-field-disabled .mat-form-field-underline

================================================================================================================================
v0.0.31 changes
1) npm -> styles.scss -> modify -> all .with-fixed-subheader classes
2) npm -> styles.scss -> add .font-16-weight-600,
                             .font-16-weight-600-blue,
                             .font-12-weight-600-blue

================================================================================================================================
v0.0.30 changes
1) npm -> styles.scss -> modify  .table-responsive th.mat-header-cell:first-of-type,
                                .table-responsive td.mat-cell:first-of-type,
                                .table-responsive td.mat-footer-cell:first-of-type,
                                .table-responsive th.mat-header-cell:last-of-type,
                                .table-responsive td.mat-cell:last-of-type,
                                .table-responsive td.mat-footer-cell:last-of-type,
                                .table-responsive th.mat-header-cell,
                                .table-responsive td.mat-cell,
                                .table-responsive td.mat-footer-cell {
                                    padding: 0 10px 0 10px;  --->  padding: 10px;
                                }
2) npm -> styles.scss -> add .content-detail-gap, .content-detail-gap.content-detail-left, .content-detail-gap.content-detail-right
3) npm -> styles.scss -> modify .with-fixed-subheader-1-line { margin-top: 38px !important; }  -->  .with-fixed-subheader-1-line { margin-top: 41px !important; }
4) npm -> styles.scss -> add .mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:hover, 
                             .mat-expansion-panel.todo-expansion.detail-card.expansion .mat-expansion-panel-header.bg-light-blue:not(hover)
5) npm -> styles.scss -> .todo-expansion.detail-card.expansion
6) npm -> styles.scss -> add .with-fixed-subheader-mat-icon-1-line,
                             .with-fixed-subheader-mat-icon-2-line,
                             .with-fixed.subheader-mat-icon-1-line-with-search,
                             .with-fixed-subheader-mat-icon-2-line-with-search,
                             .with-fixed-subheader-with-search
================================================================================================================================

v0.0.29 changes
1) npm -> ifca standard floating-btn merge with float-btn (system admin -> access security -> user -> user listing)
3) npm -> styles.scss -> .mat-card-header become padding: 10px 12px;
4) npm -> styles.scss -> add .with-fixed-subheader-with-search
5) system admin -> permission.component.ts -> <div> become <mat-card-title>
6) npm -> styles.scss -> remove .notification-content-padding (all lease applications my-profile.component.ts)
7) tenant-app add .responsive-content { width: 100% } in style.override.scss
8) tenant-app remove .page-content { margin-top: 55px } from style.override.scss
9) tenant-app add @media (min-width: 768px) -> .page-content { margin-top: unset; padding-top: 55px !important; }
10) tenant-app add @media screen and (min-width: 768px)-> .responsive-content { height: calc(100% - 165px) }
11) npm -> styles.scss -> remove .cdk-overlay-pane { max-width: 90vw; }
12) npm -> styles.scss -> remove .detail-card .info-container-meter, .detail-card .container-submeter, .container-submeter .title-meter-info-xl, .container-submeter .title-meter-info-l,
.container-submeter .title-meter-info, .info-container-meter:last-child .detail-card .container-submeter (moved to tenant-app -> style.override.scss)
13) npm -> styles.scss -> .mat-dialog-container remove box-shadow: none;
13) tenant-app -> sr-listing-filter.component.ts -> remove class="padding-0"
14) npm -> styles.scss -> .sub-header-title change font-weight become 500
15) npm -> styles.scss -> .radio-btn-border-btm grouped with .quotation-list-container
16) npm -> styles.scss -> add .quotation-list-container:last-child, .quotation-list-container:first-child